from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from sqlalchemy import text

from functions import app
from models import User

from models._helpers import *

@app.route('/dashboard', methods=['GET'])
def dashboard():

    login_user(User.query.get(1))
    args = {
            'gift_card_eligible': True,
            'cashout_ok': True,
            'user_below_silver': current_user.is_below_tier('Silver'),
    }
    return render_template("dashboard.html", **args)

@app.route('/community', methods=['GET'])
def community():
    
    login_user(User.query.get(1))
    ######################
    #
    # Render our community route using raw MySQL
    #
    ######################
    
    sql = text('''
                SELECT u.display_name, u.tier, u.point_balance, r_u.attribute, r_u_m.attribute FROM user u 
                LEFT JOIN rel_user r_u ON (u.user_id = r_u.user_id) 
                LEFT JOIN rel_user_multi r_u_m ON(u.user_id = r_u_m.user_id)
                ORDER BY u.signup_date DESC LIMIT 5
                ''')
    users = db.engine.execute(sql)
    return render_template("community_raw.html", users = users )    
    
    
    ######################
    #
    # Render our community route using SQLAlchemy's built-in ORM methods
    #
    ######################
    """
    users =  User.query.order_by(User.signup_date.desc()).limit(5).all()
    return render_template("community.html", users = users )
    """