from tests import OhmTestCase


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []
    '''
    Patrick's Tests
    '''
    def test_get_attribute(self):
        assert self.chuck.get_attribute("location") == 'EUROPE'
        assert self.justin.get_attribute("location") == 'USA'
    def test_full_name(self):
        assert self.chuck.full_name() == 'Chuck Norris'