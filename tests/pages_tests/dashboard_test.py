#########
#
# Patrick's Tests
# Add Get test for /community route to test if table is present
#
#########
from app_main import app
from tests import OhmTestCase


class DashboardTest(OhmTestCase):
    def test_get(self):
        with app.test_client() as c:
            response = c.get('/dashboard')
            response_c = c.get('/community')
            assert "Ready to begin assessment" in response.data
            assert "Display Name" in response_c.data
            