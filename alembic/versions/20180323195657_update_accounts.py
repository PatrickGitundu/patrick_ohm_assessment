"""update_accounts to reflect the changes requested

Revision ID: 376d443ba247
Revises: 00000000
Create Date: 2018-03-23 19:56:57.569656

"""

# revision identifiers, used by Alembic.
revision = '376d443ba247'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute('''UPDATE user
        SET point_balance=1000
        WHERE user_id = 1
    ''')
    
    op.execute('''INSERT INTO rel_user (user_id, rel_lookup, attribute)
        VALUES (2, 'LOCATION', 'USA')
    ''')

    op.execute('''UPDATE user
        SET tier='Silver'
        WHERE user_id = 3
    ''')


def downgrade():
    pass
